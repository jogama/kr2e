#!/usr/bin/env sh

CFLAGS="-std=c99 -Wall -Wextra -Wwrite-strings -Wpedantic -Wformat=2 -Werror -I".

# Link with -lm if using math. Not sure if this is portable
grep "math.h" $1 > /dev/null
if [ $? -eq 0 ] ; then
    gcc $CFLAGS $1 -lm
else
    gcc $CFLAGS $1
fi

exit $?
