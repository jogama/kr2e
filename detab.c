/* Exercise 1-20. Write a program detab that replaces tabs in the
input with the proper number of blanks to space to the next tab
stop. Assume a fixed set of tab stops, say every n columns.  Should n
be a variable or a symbolic parameter? */

#include <stdio.h>

int getline(char line[], int lim);
char * translate_tabs_to_spaces(const char * instring, char * outstring);
char * translate_character(char * string, const char sought, const char replacement);

#define MAXLINE 1000 /* maximum input line length */
#define TAB_WIDTH 4  /* define "n", which should be a symbolic
			parameter (explained in §1.4) because it's
			never changed. */

/* As with the current implementation of exercises 1-[17, 18],
   immediately echo edited lines.  */
int main(){
  char line[MAXLINE];    /* Current input line */
  char detabbed_line[MAXLINE * TAB_WIDTH];
  
  while (getline(line, MAXLINE) > 0)
    printf("%s", translate_tabs_to_spaces(line, detabbed_line));
}

/* getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* Translate tabs to spaces.

   We don't know how many tabs the input string has; the output string
   could be as much as TAB_WIDTH times longer than the input
   string. Thus, we request outstring, which should be able to hold
   all these tabs.  */
char * translate_tabs_to_spaces(const char * instring, char * outstring){
  int j = 0;  /* output string index */

  /* Loop over entire input string */
  for (int i = 0; instring[i] != '\0'; i++){
    if (instring[i] == '\t')  /* if found tab, */
      /* Set the next TAB_WIDTH characters to spaces. */
      for (int k=0; k < TAB_WIDTH; k++)
	outstring[j+k] = ' ';
    else
      outstring[j] = instring[i];  /* Else, copy the charcter. */

    /* Increment j */
    j += instring[i] == '\t' ? TAB_WIDTH : 1;
  }
  
  outstring[j] = '\0';
  return outstring;
}
