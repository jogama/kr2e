/* Exercise 1-21. Write a program entab that replaces strings of
blanks by the minimum number of tabs and blanks to achieve the same
spacing. Use the same tab stops as for detab . When either a tab or a
single blank would suffice to reach a tab stop, which should be given
preference? */

/* When either tab or single blank would suffice, prefer tab. Other
   programs may interpret '\t' differently.

   It would be simpler to simply return the given string if TAB_WIDTH
   is one. However, replacint ' ' with '\t' is more correct. 

   As in ex1-08.c, I interpret "blanks" as "spaces. */

#include <stdio.h>

int getline(char line[], int lim);
char * translate_spaces_to_tabs(const char * instring, char * outstring);

#define MAXLINE 1000 /* maximum input line length */
#define TAB_WIDTH 4  /* define "n", which should be a symbolic
			parameter (explained in §1.4) because it's
			never changed. */

/* As with the current implementation of exercises 1-[17, 18],
   immediately echo edited lines.  */
int main(){
  char line[MAXLINE];    /* Current input line */
  char entabbed_line[MAXLINE];
  
  while (getline(line, MAXLINE) > 0)
    printf("%s", translate_spaces_to_tabs(line, entabbed_line));
}

/* getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* Translate spaces to tabs */
char * translate_spaces_to_tabs(const char * instring, char * outstring){
  int j = 0;            /* output string index */
  int blank_count = 0;  /* Has range [0, TAB_WIDTH] */
  char c;               /* current character in loop */

  /* Loop over entire input string */
  for (int i = 0; (c = instring[i]) != '\0'; i++){
    /* Record blank. If we have enough blanks for a tab, replace it
       with tab and reset the counter. */
    if (c == ' '){
      blank_count++;

      if (blank_count == TAB_WIDTH){
	blank_count = 0;
	outstring[j] = '\t';
	j++;
      }
    }
    else{
      /* c is a nonblank. Copy over all blanks, reset the counter and
	 then copy c. */
      for (int k = 0; k < blank_count; k++, j++)
	outstring[j] = ' ';
      
      blank_count = 0;
      outstring[j] = c;
      j++;
    }
  }
  
  outstring[j] = '\0';
  return outstring;
}
