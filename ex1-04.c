#include <stdio.h>
/* print Celsius-Fahrenheit table 
   for celsius = 0, 20, ..., 300; floating-point version */
int main() {
  float fahr, celsius;
  float lower, upper, step;
  lower = 0;
  upper = 300;
  step = 20;

  /* print heading for ex1-3 */
  printf("TEMPERATURE CONVERSION TABLE\n");
  printf("Celsius  Fahrenheit\n");
  
  /* lower limit of temperature scale */
  /* upper limit */
  /* step size */
  celsius = lower;
  while (celsius <= upper) {
    fahr = (9.0/5.0) * celsius + 32.0;
    printf("%7.0f %6.1f\n", celsius, fahr);
    celsius= celsius + step;
  }
}
