#include <stdio.h>
/* Exercise 1-5. Modify the temperature conversion program to print
   the table in reverse order, that is, from 300 degrees to 0. */
int main() {
  float lower, upper, step;
  lower = 0;
  upper = 300;
  step = 20;

  /* print heading for ex1-5 */
  printf("TEMPERATURE CONVERSION TABLE\n");
  printf("Fahrenheit  Celsius\n");
  
  /* lower limit of temperature scale */
  /* upper limit */
  /* step size */
  for(float celsius, fahr=upper; fahr >= lower; fahr -= step){
    celsius = (5.0/9.0) * (fahr-32.0);
    printf("%7.0f %6.1f\n", fahr, celsius);
  }
}
