#include <stdio.h>
/* Exercise 1-6. Verify that the expression
   getchar() != EOF is 0 or 1. */
int main(){
  /* stackoverflow.com/a/15796931 Once I started wondering whether the
     text was asking us to look at the ISO C standard text to verify
     the possible outputs of the != operator, I went to
     stackoverflow. The answer ther was moderately helpful: */
  int c = (getchar() != EOF);
  printf("%d\n", c);
  /* but does not satisfy me. I'm not sure how to appreciate or learn
     from this exercise. */
}
