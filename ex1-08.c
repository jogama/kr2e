#include <stdio.h>

/* Exercise 1-8. Write a program to count blanks, tabs, and
   newlines.
   I interpret "blanks" as "spaces". */
int main(){
  int c, bk, tb, nl;
  bk = tb = nl = 0;
  
  while( (c = getchar()) != EOF){
    printf("newlines: %2d. blanks: %2d. tabs: %2d.",
	 nl, bk, tb);  
    switch(c){
    case '\n': nl++; break;
    case '\t': tb++; break;
    case ' ':  bk++;
    }
    for(int i=0;i<29+3*2;i++) printf("\b");
  }
}
