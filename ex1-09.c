#include <stdio.h>
/* Exercise 1-9. Write a program to copy its input to its output,
   replacing each string of one or more blanks by a single blank.*/
int main(){
  int c, blank_count;
  blank_count = 0;

  /* We must delay printing of a chacter by one iteration */
  while((c = getchar()) != EOF){
    if(c == ' ')
      blank_count = 1;
    else if(blank_count == 0)
      printf("%c", c);
    else if(blank_count == 1){
      printf(" ");
      printf("%c", c);
      blank_count = 0;
    }
  }
}
