#include <stdio.h>

/* Exercise 1-12. Write a program that prints its input one word per
   line.
*/

int main(){
  int c;
  int previous_c = 'z';
  
  while((c = getchar()) != EOF){
    /* Ignore consecutive blanks.  */
    if(c == ' ' && previous_c != ' ')
      printf("\n");
    else if(c == ' ' && previous_c == ' ')
      continue;
    else
      putchar(c);    
    previous_c = c;
  }
}
