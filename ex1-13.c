#include <stdio.h>
#include <stdbool.h>
#include <math.h>

/* Exercise 1-13. Write a program to print a histogram of the lengths
   of words in its input. It is easy to draw the histogram with the
   bars horizontal; a vertical orientation is more challenging. */

int main(){
  /* I believe the longest words in english are names of chemical
     compounds in chemistry. */
  const int MAX_WORD_LENGTH = 5;
  const char * delimiters = ";,. \n\t";
  int c, word_length;
  bool is_delimiter;

  /* This array size allows us to have the length of the word
     correspond to the index of the array. */
  unsigned long histogram[MAX_WORD_LENGTH+2];

  /* Initialize histogram an other variables to zero */
  for (int i=0; i<MAX_WORD_LENGTH; i++)
    histogram[i] = 0;
  word_length = 0;

  /* Get the data */
  while ((c = getchar()) != EOF){

    /* Check if c is a delimiter */
    is_delimiter = false;
    for(int d=0; delimiters[d] != '\0'; d++)
      if(c == delimiters[d]){
	is_delimiter = true;
	break;
      }
    
    if(is_delimiter){
      /* Add the word to the histogram and reset the counter */
      if (word_length <= MAX_WORD_LENGTH)
        histogram[word_length]++;
      else
	/* The last bin in the histogram is for extremely long words */
	histogram[MAX_WORD_LENGTH+1]++;
      word_length = 0;
    }
    else
      /* This is a character in a word */
      word_length++;
  }

  /* Then print the data as a histogram */
  printf("HISTOGRAM\n");
  int width = ceil(log10(MAX_WORD_LENGTH));
  const char bar = '*';

  /* Even though we counted them, we don't print the quantity of
     repeated delimiters, because they aren't considered words. We
     instead start at the quantity of words of lenth=1 */
  for(int length=1; length<=MAX_WORD_LENGTH+1; length++){
    /* Dont' print empty data */
    if(histogram[length] != 0){
      if(length == MAX_WORD_LENGTH+1)
	/* Greater than the maximum word length. Omit space for
	   alignment.*/
	printf(">%*d", width, length - 1);
      else 
	printf("%*d ", width, length);

      /* Print one bar character for each word of length length. */
      for(unsigned i=0; i<histogram[length]; i++)
	putchar(bar);
      printf("\n");
    }
  }
}
