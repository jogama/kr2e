#include <stdio.h>
#include <stdbool.h>

/* Exercise 1-14. Write a program to print a histogram of the
   frequencies of different characters in its input. */

#define ASCII 128

int main(){
  int c;

  /* This array size allows us to count non-ASCII characters in the
     last bin of the array. */
  unsigned long histogram[ASCII+2];

  /* Initialize histogram to zero */
  for (int i=0; i<ASCII+1; i++)
    histogram[i] = 0;

  /* Get the data */
  while ((c = getchar()) != EOF){
    if(c <= ASCII)
      histogram[c]++;
    else
      histogram[ASCII+1]++;
  }

  /* Then print the data as a histogram */
  printf("HISTOGRAM\n");
  const char bar = '*';

  /* Handle characters in allowed character set */
  for(c=0; c<=ASCII; c++){
    /* Don't print empty data */
    if(histogram[c] != 0){
      printf("%c", c);
      
      /* Print one bar character for each character c */
      for(unsigned i=0; i<histogram[c]; i++)
	putchar(bar);
      printf("\n");
    }
  }

  /* Handle characters outside allowed character set */
}
