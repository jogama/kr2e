/* Exercise 1.15. Rewrite the temperature conversion program of
   Section 1.2 to use a function for conversion. */

#include <stdio.h>

int fahrenheit_to_celcius(int fahr);

/* print Fahrenheit-Celsius table
   for fahr = 0, 20, ..., 300 */
int main(){
  int fahr, celsius;
  int lower, upper, step;
  lower = 0;
  upper = 300;
  step = 20;
  /* lower limit of temperature scale */
  /* upper limit */
  /* step size */
  fahr = lower;
  while (fahr <= upper) {
    celsius = fahrenheit_to_celcius(fahr);
    printf("%d\t%d\n", fahr, celsius);
    fahr = fahr + step;
  }

  return 0;;
}

/* Convert temperature from Fahrenheit to Celsius. */
int fahrenheit_to_celcius(int fahr){
  return 5 * (fahr-32) / 9;
}
