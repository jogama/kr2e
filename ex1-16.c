/* Exercise 1-16. Revise the main routine of the longest-line program
   so it will correctly print the length of arbitrary long input
   lines, and as much as possible of the text. */

#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */

int getline(char line[], int maxline);
void copy(char to[], char from[]);

/* print the longest input line */
int main() {
  int len;               /* current line length */
  int max;               /* maximum length seen so far */   
  char line[MAXLINE];    /* current input line */
  char longest[MAXLINE]; /* longest line saved here */

  max = 0;
  while ((len = getline(line, MAXLINE)) > 0)
    if (len > max) {
      /* All lines returned by geline have either
	    (a length less than MAXLINE-1 AND end with "\n\0")
	 OR (a length equal to MAXLINE-1 and *do not* end with "\n\0") */
      /* (one of these conditions could be removed. I'm not sure what would be best practice.) */
      if (len == MAXLINE-1 && line[len-1] != '\n')
	/* Then len returned by getline is inaccurate. Keep counting. */
	for (int c; (c=getchar()) != EOF && c != '\n'; ++len) ;
      max = len;
      copy(longest, line);
    }
  if (max > 0) /* there was a line */
    printf("Longest line has length of %i:\n%s", max, longest);
  return 0;
}

/* getline: read a line into s, return length */
int getline(char s[],int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
  int i;

  i = 0;
  while ((to[i] = from[i]) != '\0')
    ++i;
}
