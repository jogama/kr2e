/* Exercise 1-17. Write a program to print all input lines that are
   longer than 80 characters. */

#include <stdio.h>

#define THRESHHOLD_LENGTH 80 /* Lines with more than this qtty of characters will be recorded */
#define MAXLINE 1000 /* maximum input line length */

int getline(char line[], int maxline);
void copy(char to[], char from[]);

/* Immediately echo any lines that are longer than 80 characters. */
int main(){
  int len;               /* current line length */
  char line[MAXLINE];    /* current input line */

  while ((len = getline(line, MAXLINE)) > 0)
    if (len > THRESHHOLD_LENGTH)
      printf("%s", line);
}

/* getline: read a line into s, return length */
int getline(char s[],int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
  int i;

  i = 0;
  while ((to[i] = from[i]) != '\0')
    ++i;
}
