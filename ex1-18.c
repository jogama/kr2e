/* Exercise 1-18. Write a program to remove trailing blanks and tabs
   from each line of input, and to delete entirely blank lines. */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define MAXLINE 1000 /* maximum input line length */

/* Header files aren't discussed till ch4 or ch7, so we won't use them
   yet and will copy-paste these functions from ex1-17. */
int getline(char line[], int lim);
char * trim_whitespcace(char * destination, const char * line, int len);

/* As with the current implementation of exercise 1-17, immediately
   echo edited lines. We haven't reached malloc, structs, pointers, or
   command line aguments with which to do much more. */
int main(){
  int len;               /* Current line length */
  char line[MAXLINE];    /* Current input line */
  char stripped_line[MAXLINE]; /* Buffer for new line with no trailing whitespace */
  
  while ((len = getline(line, MAXLINE)) > 0)
    printf("%s", trim_whitespcace(stripped_line, line, len));
}

/* getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}


/* Copy a string with no whitespace at the end */
char * trim_whitespcace(char * destination, const char * line, int len){
 /* walk it in reverse */
  int whitespace_count = 0;
  
  for (int index=len-1; index>=0; index--)
    if (line[index] == ' ' || line[index] == '\t' || line[index] == '\n')
      whitespace_count += 1;
    else
      break; 
  
  /* Handle case of a line being entirely made up of whitespace */
  if (whitespace_count == len)
    destination[0] = '\0';
  else{
    /* We redefine len as length of return string string */
    len -= whitespace_count;
    strncpy(destination, line, len);
    destination[len] = '\n';
    destination[len+1] = '\0';
  }
  return destination;
}
