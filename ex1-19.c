/* Exercise 1-19. Write a function reverse(s) that reverses the
character string s. Use it to write a program that reverses its input
a line at a time. */

#include <stdio.h>

int getline(char line[], int lim);
int string_length(const char * s);
char * reverse(char * string);

#define MAXLINE 1000 /* maximum input line length */

/* As with the current implementation of exercises 1-[17, 18],
   immediately echo edited lines.  */
int main(){
  int len;               /* Current line length */
  char line[MAXLINE];    /* Current input line */
  
  while ((len = getline(line, MAXLINE)) > 0)
    printf("%s", reverse(line));
}

/* getline: read a line into s, return length */
int getline(char s[], int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}



/* Return length of the string */
int string_length(const char * s){
  int len;
  for (len=0; s[len] != '\0'; len++) ;
  return len;
}


/* Reverse the given string in place */
char * reverse(char * string){
  int length = string_length(string);  /* Includes '\n' and excludes '\0' */
  char tmp;

  /* Iterate until the midway point of the first and antepenultimate
     characters. Assumes that the last two characters are "\n\0", and
     that we want the reversed string to terminate with this
     substring. */
  for (int i=0; i < (length - 1) / 2; i++){
    tmp = string[i];
    string[i] = string[length - i - 2];
    string[length - i - 2] = tmp;
  }
  
  return string;
}
