/* Exercise 1-22. Write a program to ``fold'' long input lines into two or more shorter lines after
the last non-blank character that occurs before the n-th column of input. Make sure your
program does something intelligent with very long lines, and if there are no blanks or tabs
before the specified column. */

/*
   getline probably shouldn't be its own function for this? I'd like
   to store all the indices of blanks in an incoming line. I'd rather
   not have to pass it back to the caller.

   As with the previous programs in this chapter, this will operate on
   stdin and print to stdout.
*/

/*
bugs:

stringlength == DESIRED_LINE: get extraneous '\n'.

'aaaa
'a a a a' -> 'a a a\na'
'aaaa' -> 'aaaa\na'
'aaaaa' -> 'aaaaa' # also for six a's.
'a a a a a' -> 'a a a a a\n\nV'
'aaaa aaaa' -> 'aaaa aaaa\n\nV'

 */

#include <stdio.h>

#define MAX_LINE 16   /* Assume there are no lines longer than 1000 characters*/
#define DESIRED_LINE 4 /* The desired line length to fold to. */

/* Utility functions */
int fold(const char * inp, char * out, const  _Bool * allowed_fold_points, const int len);
int nearest_truth(const _Bool * arr, const int minimum_index, const int ideal_index);
int prompt_failed_fold();

int main(){
  int len;               /* Current line length */
  char input_line[MAX_LINE+1];    /* Current input line + '\0' */
  _Bool blanks[MAX_LINE];
  char output_line[MAX_LINE+1];   /* Current output line */

  /* Loop until reached end of file */
  char c = 1;
  while (c != EOF){
    /* Read input string into buffer `input_line` to get line length */
    for (len=1; // Length is at least one; lines always contain '\n'.
           len < MAX_LINE
	   && (c=getchar()) != EOF
	   && c != '\n';
	 ++len){
      
      input_line[len-1] = c;  /* Copy char from line into buff */
      blanks[len-1] = c == ' ' || c == '\t'; /* Store blanks */
    }

    /* "Make sure your program does something intelligent with very
    long lines" I'm not certain what is meant by "long." Once the
    input buffer is full; main() stops accepting new characters,
    processes those received, then continues processing the current
    line. It will jarr the user, but I don't know think of a better
    way to handle this without dynamic memory. The following if
    handles the cases of lines with length > MAX_LINE as well as the
    normal '\n' case. */
    if (c != EOF) {
      input_line[len-1] = c;
      input_line[len] = '\0';
    }
    else
      input_line[len-1] = '\0';
    

    /* Fold the input line the output buffer */
    if(!fold(input_line, output_line, blanks, len)){
      /* fold() failed. There are no blanks or tabs before the
         specified column.
         
	 "Make sure your program does something intelligent ... if
         there are no blanks or tabs before the specified column."

	 I think best would be to make a prompt asking whether to
	 continue or exit.
      */

      if(prompt_failed_fold() == 1)
	/* The user desires to exit. */
	return 1;
    }

    printf("%s", output_line);
  }
  
  return 1;
}

/* Return 1 if success, 0 otherwise.

"``fold'' long input lines into two or more shorter lines after the
last non-blank character that occurs before the n-th column of input."

for very long lines:
  ¿ I don't see why long lines are a different case, unless they're
  longer than MAX_LINE.

if there are no blanks or tabs before the specified column:
  Return 0 for this failure mode and let the caller handle it.

arguments:
  inp: input string
  out: output string buffer, expected to be of same size as input
  allowed_fold_points: a boolean array parallel to inp. For this
    program, True/1 denotes a blank or tab.
  len: length of inp and index of '\0' in inp.
*/
int fold(const char * inp, char * out, const  _Bool * allowed_fold_points, const int len){
  int i;
  for(i=0; inp[i] != '\0' && i < MAX_LINE; ++i)
    out[i] = inp[i];
  out[i] = '\0';

  /* Handle trivial case by returning the copied string */
  if(len <= DESIRED_LINE){
    return 1;
  }

  /* Count number of substrings to fold into */
  const int substring_count = DESIRED_LINE / len + ((DESIRED_LINE % len) > 0);

  /* Check if possible */
  int crease_point_count = 0;
  for(int i = 0; i<len; crease_point_count += allowed_fold_points[i], ++i) ;
  if (crease_point_count < substring_count - 1) return 0;

  /* find indices to replace with newlines. */
  for(int substring=1, min=0, index=0; substring <= substring_count; ++substring){
    index = substring * len / substring_count; /* substring * substring length */
    index = nearest_truth(allowed_fold_points, min, index);
    out[index] = '\n';
    min = index;
  }

  return 1;
}

/* 
args:
  arr: boolean array of ones and zeros
  ideal_index: 

returns:

index of nearest truth less than or equal to ideal_index.
 -1 if there are only zeros and no truth values
 -2 if some unexpected failure occurs

assumptions:
  arr is a valid array
  ideal_index is less than or equal to `length(arr) - 1`.
  minimum_index is in the range [0, length(arr)).
*/
int nearest_truth(const _Bool * arr, const int minimum_index, const int ideal_index){
  if(ideal_index < minimum_index)
    return -1;
  else if(arr[ideal_index])
    return ideal_index;
  else
    return nearest_truth(arr, minimum_index, ideal_index - 1);
  
  return -2;
}

/* Allows the user to provide direction as to how move forward upon
   fold() failure. Loops until the user supplies a valid response.
returns:
  1: If exit
  2: Continue

*/
int prompt_failed_fold(){
  char response;

  /* TODO: justify the below line. */
  printf("\nI cannot fold the most recent line; it does not have enough blanks before column %i. Would you like to ", DESIRED_LINE);
  printf("\n    1) Exit");
  printf("\n    2) Continue, leaving the line unfolded? ");
  while(1){
    printf("\nType 1 or 2: \n");
    response = getchar();
    if (response == '1'){
      printf("\nExiting...\n");
      return 1;
    } else if (response == '2'){
      printf("\nContinuing...\n");
      return 2;
    } else
      printf("\nInvalid response. Enter '1' or '2'. ");
  }
}
