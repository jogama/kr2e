/* Exercise 1-24. Write a program to check a C program for rudimentary
  syntax errors like unmatched parentheses, brackets and braces. Don't
  forget about quotes, both single and double, escape sequences, and
  comments. (This program is hard if you do it in full generality.)
 */

/*

  This is basically a parser. The first three chapters of the dragon
  book should help me with this. But I did have a feeling of dread to
  think that I have to read it; those three chapters comprise 189
  pages. K&R C itself is about as long as those three chapters.

  ex1-23 is similar, but not as intense.

  I want to be able to say that I study robots before I study
  computers. To what extent does reading the first three chapters of
  the dragon book push me towards that goal? Parsing/message passing
  of nodes in a network, perhaps?

  I think it would be good to go to a non compsci book after this one.

  I just realized that one way to test this is by taking all the c
  code here, making sure it can compile, then *finally* taking this
  opportunity to learn how to use a fuzzer. 
  
 */
